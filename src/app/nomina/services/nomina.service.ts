import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable, Subject, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { GLOBAL } from '../../services/global';

@Injectable()
export class NominaService {
    public url: string;

    constructor(
        public _http: Http
    ) {
        this.url = GLOBAL.url;
    }

    crearNomina(mes: string, ano: string) {
        return this._http.get(this.url + '/crea/nomina/mes/' + mes + '/ano/' + ano + '').pipe(map(res => res.json()));
    }

}
