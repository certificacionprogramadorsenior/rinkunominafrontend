import { Component, OnInit } from '@angular/core';
import { NominaService } from '../services/nomina.service';
import { GLOBAL } from '../../services/global';
import swal from 'sweetalert2';

@Component({
    selector: 'app-generarnomina',
    templateUrl: './generarNomina.component.html',
    styleUrls: ['./generarNomina.component.css'],
    providers: [NominaService]
})
export class GenerarNominaComponent implements OnInit {
    //#region Variables
    public fecha: Date;
    public fechaHoy: string;
    public mes: string;
    public ano: string;
    //#endregion Variables

    //#region Constructores
    constructor(
        private _nominaService: NominaService
    ) {
        this.fecha = new Date();
        this.mes = (this.fecha.getMonth() + 1).toString();
        this.ano = this.fecha.getFullYear().toString();
        this.fechaHoy = this.ano + '-' + this.mes;
    }

    ngOnInit() { }
    //#endregion Constructores

    //#region Consultas
    generarNomina() {
        this._nominaService.crearNomina(this.mes, this.ano).subscribe(
            response => {
                if (response.code === 200) {
                    window.open(GLOBAL.urlPDF + 'nomina-' + this.mes + '-' + this.ano + '.pdf', '_blank');
                } else {
                    swal({
                        type: 'info',
                        title: 'No hay movimientos registrados este mes'
                    });
                }
            },
            error => { }
        );
    }
    //#endregion Consultas

    //#region Metodos Form
    cambiarFecha(fecha: string) {
        this.ano = fecha.substr(0, 4);
        this.mes = fecha.substr(5, 2);
    }
    //#endregion Metodos Form

}
