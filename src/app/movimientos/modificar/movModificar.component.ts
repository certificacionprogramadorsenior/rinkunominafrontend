import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MovimientoService } from '../services/movimiento.service';
import { Movimiento } from '../models/movimiento';
import swal from 'sweetalert2';

@Component({
    selector: 'app-movmodificar',
    templateUrl: './movModificar.component.html',
    styleUrls: ['./movModificar.component.css'],
    providers: [MovimientoService]
})
export class MovModificarComponent implements OnInit {
    //#region Variables
    public id: string;
    public movimiento: Movimiento;
    public toast = swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
    });
    //#endregion Variables

    //#region Constructores
    constructor(
        private _movimientoService: MovimientoService,
        private _route: ActivatedRoute,
        private _router: Router
    ) {
        this.movimiento = new Movimiento('', '', '', '', '', '', '', '0', '0');
    }

    ngOnInit() {
        this.consultarMovimiento();
    }
    //#endregion Constructores

    //#region Consultas
    consultarMovimiento() {
        this._route.params.forEach((params: Params) => {
            this.id = params['id'];
        });
        this._movimientoService.consultarMovimiento(this.id).subscribe(
            response => {
                if (response.code === 200) {
                    this.movimiento = response.data[0];
                } else {
                    this.toast({
                        type: 'error',
                        title: 'Error al consultar el empleados.'
                    });
                }
            },
            error => {
                this.toast({
                    type: 'error',
                    title: 'Error revisar servidor.'
                });
            }
        );
    }

    actualizarMovimiento() {
        this._movimientoService.actializarMovimiento(this.movimiento).subscribe(
            response => {
                if (response.code === 200 && response.data) {
                    swal({
                        type: 'success',
                        title: 'Movimiento actualizado correctamente',
                        timer: 2500
                    });
                    setTimeout(() => {
                        this._router.navigate(['/movimiento/buscar']);
                    }, 2700);
                } else {
                    this.toast({
                        type: 'error',
                        title: 'Error actualizando el Movimiento.'
                    });
                }
            },
            error => {
                this.toast({
                    type: 'error',
                    title: <any>error
                });
            }
        );
    }

    consultarEmpleado() {
        if (this.movimiento.clave) {
            this._movimientoService.consultarEmpleado(this.movimiento.clave).subscribe(
                response => {
                    if (response.code === 200 && response.data != null) {
                        this.movimiento.nombre = response.data[0].nombre;
                        this.movimiento.rol = response.data[0].rol;
                        this.movimiento.tipo = response.data[0].tipo;
                    } else {
                        this.toast({
                            type: 'error',
                            title: 'Error el empleado no existe.'
                        });
                        this.movimiento.clave = '';
                    }
                },
                error => {
                    this.toast({
                        type: 'error',
                        title: <any>error
                    });
                }
            );
        } else {
            this.toast({
                type: 'error',
                title: 'Ingrese un usuario.'
            });
        }
    }
    //#endregion Consultas

    //#region Metodos Front
    onSubmitActualizar() {
        this.actualizarMovimiento();
    }
    //#endregion Metodos Front

    //#region Metodos
    cambiarFecha(fecha: string) {
        this.movimiento.fecha = fecha;
    }
    //#endregion Metodos

    //#region Metodos validadores de caracteres
    soloNumeros(e: any) {
        const key = window.event ? e.which : e.keyCode;
        if (key < 48 || key > 57) {
            e.preventDefault();
        }
    }

    pulsarEnter(e: any) {
        if (e.keyCode === 13 && !e.shiftKey) {
            e.preventDefault();
            this.consultarEmpleado();
        }
    }
    //#endregion Metodos validadores de caracteres

}
