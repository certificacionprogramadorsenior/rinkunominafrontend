import { Component, OnInit } from '@angular/core';
import { Movimiento } from '../models/movimiento';
import { NgForm } from '@angular/forms';
import swal from 'sweetalert2';
import { MovimientoService } from '../services/movimiento.service';

@Component({
    selector: 'app-movnuevo',
    templateUrl: './movNuevo.component.html',
    styleUrls: ['./movNuevo.component.css'],
    providers: [MovimientoService]
})
export class MovNuevoComponent implements OnInit {
    //#region Variables
    public movimiento: Movimiento;
    public fechaG: string;
    public toast = swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
    });
    //#endregion Variables

    //#region Constructores
    constructor(
        private _movimientoService: MovimientoService
    ) {
        this.movimiento = new Movimiento('', '', '', '', '', '', '8', '0', '0');
        this.consultarFecha();
    }

    ngOnInit() { }
    //#endregion Contructores

    //#region Consultas
    consultarFecha() {
        this._movimientoService.consultarFecha().subscribe(
            response => {
                this.fechaG = '';
                if (response.code === 200) {
                    this.fechaG = response.data[0].fecha;
                } else {
                    this.toast({
                        type: 'error',
                        title: 'Error consultando la fecha.'
                    });
                }
                this.movimiento = new Movimiento('', '', '', '', '', this.fechaG, '8', '0', '0');
            },
            error => {
                this.toast({
                    type: 'error',
                    title: <any>error
                });
            }
        );
    }

    consultarEmpleado() {
        if (this.movimiento.clave) {
            this._movimientoService.consultarEmpleado(this.movimiento.clave).subscribe(
                response => {
                    if (response.code === 200 && response.data != null) {
                        this.movimiento.nombre = response.data[0].nombre;
                        this.movimiento.rol = response.data[0].rol;
                        this.movimiento.tipo = response.data[0].tipo;
                    } else {
                        this.toast({
                            type: 'error',
                            title: 'Error el empleado no existe.'
                        });
                        this.movimiento.clave = '';
                    }
                },
                error => {
                    this.toast({
                        type: 'error',
                        title: <any>error
                    });
                }
            );
        } else {
            this.toast({
                type: 'error',
                title: 'Ingrese un usuario.'
            });
        }
    }

    crearMovimiento() {
        this._movimientoService.crearMovimiento(this.movimiento).subscribe(
            response => {
                if (response.code === 200 && response.data) {
                    swal({
                        type: 'success',
                        title: 'Movimiento guardado correctamante.',
                    });
                } else {
                    this.toast({
                        type: 'error',
                        title: 'Error al guardar el movimiento.'
                    });
                }
            },
            error => {
                this.toast({
                    type: 'error',
                    title: 'Error al guardar el movimiento.'
                });
            }
        );
    }
    //#endregion Consultas

    //#region Metodos del Form
    onSubmitMovNuevo(form: NgForm) {
        this.crearMovimiento();
        form.resetForm();
        this.reiniciarForm();
    }

    reiniciarForm() {
        setTimeout(() => {
            this.movimiento = new Movimiento('', '', '', '', '', this.fechaG, '8', '0', '0');
        }, 10);
    }
    //#endregion Metodos del Form

    //#region Metodos
    cambiarFecha(fecha: string) {
        this.movimiento.fecha = fecha;
    }
    //#endregion Metodos

    //#region Metodos validadores de caracteres
    soloNumeros(e: any) {
        const key = window.event ? e.which : e.keyCode;
        if (key < 48 || key > 57) {
            e.preventDefault();
        }
    }

    pulsarEnter(e: any) {
        if (e.keyCode === 13 && !e.shiftKey) {
            e.preventDefault();
            this.consultarEmpleado();
        }
    }
    //#endregion Metodos validadores de caracteres

}
