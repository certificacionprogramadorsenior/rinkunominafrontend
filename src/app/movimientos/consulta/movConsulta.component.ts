import { Component, OnInit } from '@angular/core';
import { Movimiento } from '../models/movimiento';
import swal from 'sweetalert2';
import { MovimientoService } from '../services/movimiento.service';

@Component({
    selector: 'app-movconsulta',
    templateUrl: './movConsulta.component.html',
    styleUrls: ['./movConsulta.component.css'],
    providers: [MovimientoService]
})
export class MovConsultaComponent implements OnInit {
    //#region Variables
    public movimiento;
    public datosCompletos: Array<Movimiento>;
    public movimientos: Array<Movimiento>;
    public toast = swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
    });
    public swalWithBootstrapButtons = swal.mixin({
        confirmButtonClass: 'btn btn-success separa_boton',
        cancelButtonClass: 'btn btn-danger separa_boton',
        buttonsStyling: false,
    });
    public claveFiltro: string;
    public nombreFiltro: string;
    public rolFiltro: string;
    public tipoFiltro: string;
    public fechaFiltro: string;
    //#endregion Variables

    //#region Constructor
    constructor(
        private _movimientoService: MovimientoService
    ) {
        this.claveFiltro = '';
        this.nombreFiltro = '';
        this.rolFiltro = '';
        this.tipoFiltro = '';
        this.fechaFiltro = '';
    }

    ngOnInit() {
        this.consultarMovimientos();
    }
    //#endregion Constructor

    //#region Consultas
    consultarMovimientos() {
        this._movimientoService.consultarMovimientos().subscribe(
            response => {
                if (response.code === 200) {
                    this.datosCompletos = response.data;
                    this.movimientos = this.datosCompletos;
                } else {
                    this.toast({
                        type: 'error',
                        title: 'Error al consultar los movimientos.'
                    });
                }
            },
            error => {
                this.toast({
                    type: 'error',
                    title: <any>error
                });
            }
        );
    }

    eliminarMovimiento() {
        this._movimientoService.eliminarMovimiento(this.movimiento.claveMovimiento).subscribe(
            response => {
                if (response.code === 200 && response.data) {
                    this.toast({
                        type: 'info',
                        title: 'Movimiento eliminado correctamente.'
                    });
                    this.borrarDeDatosCompletos();
                    this.movimientos.splice(this.movimiento.lugar, 1);
                }
            },
            error => {
                this.toast({
                    type: 'error',
                    title: <any>error
                });
            }
        );
    }
    //#endregion Consultas

    //#region Metodos Front
    preguntarBorrar(index) {
        this.movimiento = this.movimientos[index];
        this.movimiento.lugar = index;

        this.swalWithBootstrapButtons({
            title: 'Estŕ¸Łŕ¸ seguro de borrar el movimiento de?',
            html:
                '<div style="font-size:1.5rem;">' +
                'Nombre: <strong>' + this.movimiento.nombre + '</strong><br>' +
                'Rol: <strong>' + this.movimiento.rol + '</strong><br>' +
                'Tipo: <strong>' + this.movimiento.tipo + '</strong><br>' +
                'Fecha: <strong>' + this.movimiento.fecha + '</strong><br>' +
                '</div>',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Si, borralo!',
            cancelButtonText: 'No, cancela!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                this.eliminarMovimiento();
            }
        });
    }

    recontruir(cadena: string, cual: number) {
        this.movimientos = [];

        switch (cual) {
            case 1:
                this.claveFiltro = cadena;
                break;
            case 2:
                cadena = cadena.toUpperCase();
                this.nombreFiltro = cadena;
                break;
            case 3:
                this.rolFiltro = cadena;
                break;
            case 4:
                this.tipoFiltro = cadena;
                break;
            case 5:
                const ano: string = cadena.substr(0, 4);
                const mes: string = cadena.substr(5, 2);
                const dia: string = cadena.substr(8, 2);
                if (ano === '' || mes === '' || dia === '') {
                    this.fechaFiltro = '';
                } else {
                    this.fechaFiltro = dia + '/' + mes + '/' + ano;
                }
                break;
        }
        this.datosCompletos.forEach(mov => {
            if (mov.clave.includes(this.claveFiltro) &&
                mov.nombre.includes(this.nombreFiltro) &&
                mov.rol.includes(this.rolFiltro) &&
                mov.tipo.includes(this.tipoFiltro) &&
                mov.fecha.includes(this.fechaFiltro)
            ) {
                this.movimientos.push(mov);
            }
        });
    }

    //#endregion Metodos Front

    //#region Metodos
    borrarDeDatosCompletos() {
        for (let i = 0; i < this.datosCompletos.length; i++) {
            if (this.datosCompletos[i].claveMovimiento === this.movimiento.claveMovimiento) {
                this.datosCompletos.splice(i, 1);
                break;
            }
        }
    }
    //#endregion Metodos

    //#region Metodos validadores de caracteres
    soloNumeros(e: any) {
        const key = window.event ? e.which : e.keyCode;
        if (key < 48 || key > 57) {
            e.preventDefault();
        }
    }

    soloLetras(e: any) {
        const key = e.keyCode || e.which;
        const tecla = String.fromCharCode(key).toLowerCase();
        const letras = ' áéíóúabcdefghijklmnñopqrstuvwxyz';
        const especiales = ['8', '37', '39', '46'];

        let tecla_especial = false;
        especiales.forEach(x => {
            if (key === x) {
                tecla_especial = true;
            }
        });

        if (letras.indexOf(tecla) === -1 && !tecla_especial) {
            return false;
        }
    }
    //#endregion Metodos validadores de caracteres
}
