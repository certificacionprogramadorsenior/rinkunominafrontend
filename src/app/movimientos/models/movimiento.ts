export class Movimiento {

    constructor(
        public claveMovimiento: string,
        public clave: string,
        public nombre: string,
        public rol: string,
        public tipo: string,
        public fecha: string,
        public horasTrabajadas: string,
        public cantEntregas: string,
        public cubrio: string
    ) {}

}
