import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable, Subject, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { Movimiento } from '../models/movimiento';
import { GLOBAL } from '../../services/global';

@Injectable()
export class MovimientoService {
    public url: string;

    constructor(
        public _http: Http
    ) {
        this.url = GLOBAL.url;
    }

    consultarEmpleado(numero: string) {
        return this._http.get(this.url + 'consultar/empleado/mov/' + numero).pipe(map(res => res.json()));
    }

    consultarFecha() {
        return this._http.get(this.url + 'fecha').pipe(map(res => res.json()));
    }

    crearMovimiento(movimiento: Movimiento) {
        const json = JSON.stringify(movimiento);
        const params = 'json=' + json;
        const headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});

        return this._http.post(this.url + 'crear/movimiento', params, {headers: headers}).pipe(map(res => res.json()));
    }

    consultarMovimientos() {
        return this._http.get(this.url + 'consultar/movimientos').pipe(map(res => res.json()));
    }

    consultarMovimiento(numero: string) {
        return this._http.get(this.url + 'consultar/movimiento/' + numero).pipe(map(res => res.json()));
    }

    actializarMovimiento(movimiento: Movimiento) {
        const json = JSON.stringify(movimiento);
        const params = 'json=' + json;
        const headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});

        return this._http.post(this.url + 'actualiza/movimiento', params, {headers: headers}).pipe(map(res => res.json()));
    }

    eliminarMovimiento(numero: string) {
        return this._http.get(this.url + 'elimina/movimiento/' + numero).pipe(map(res => res.json()));
    }
}
