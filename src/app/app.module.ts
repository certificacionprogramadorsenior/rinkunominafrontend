import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

// HTTP
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

// Rutas
import { routing, appRoutingProviders } from './app.routing';

// Componentes
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

import { EmpNuevoComponent } from './empleados/nuevo/empNuevo.component';
import { EmpConsultaComponent } from './empleados/consulta/empConsulta.component';
import { EmpModificarComponent } from './empleados/modificar/empModificar.component';

import { MovNuevoComponent } from './movimientos/nuevo/movNuevo.component';
import { MovConsultaComponent } from './movimientos/consulta/movConsulta.component';
import { MovModificarComponent } from './movimientos/modificar/movModificar.component';

import { GenerarNominaComponent } from './nomina/generarNomina/generarNomina.component';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,

        EmpNuevoComponent,
        EmpConsultaComponent,
        EmpModificarComponent,

        MovNuevoComponent,
        MovConsultaComponent,
        MovModificarComponent,

        GenerarNominaComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        routing
    ],
    providers: [appRoutingProviders],
    bootstrap: [AppComponent]
})
export class AppModule { }
