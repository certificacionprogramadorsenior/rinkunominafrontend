import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Componentes
import { HomeComponent } from './home/home.component';

import { EmpNuevoComponent } from './empleados/nuevo/empNuevo.component';
import { EmpConsultaComponent } from './empleados/consulta/empConsulta.component';
import { EmpModificarComponent } from './empleados/modificar/empModificar.component';

import { MovNuevoComponent } from './movimientos/nuevo/movNuevo.component';
import { MovConsultaComponent } from './movimientos/consulta/movConsulta.component';
import { MovModificarComponent } from './movimientos/modificar/movModificar.component';

import { GenerarNominaComponent } from './nomina/generarNomina/generarNomina.component';

const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'home', component: HomeComponent },

    { path: 'empleado', component: EmpNuevoComponent },
    { path: 'empleado/nuevo', component: EmpNuevoComponent },
    { path: 'empleado/buscar', component: EmpConsultaComponent },
    { path: 'empleado/modificar/:id', component: EmpModificarComponent },

    { path: 'movimiento', component: MovNuevoComponent },
    { path: 'movimiento/nuevo', component: MovNuevoComponent },
    { path: 'movimiento/buscar', component: MovConsultaComponent },
    { path: 'movimiento/modificar/:id', component: MovModificarComponent },

    { path: 'generar-nomina', component: GenerarNominaComponent },

    { path: '**', redirectTo: '/home' }
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);

