export class Empleado {

    constructor(
        public clave: string,
        public nombre: string,
        public rol: string,
        public tipo: string
    ) {}

}
