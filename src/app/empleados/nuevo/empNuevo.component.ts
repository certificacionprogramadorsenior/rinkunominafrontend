import { Component, OnInit } from '@angular/core';
import { Empleado } from '../models/empleado';
import { NgForm } from '@angular/forms';
import swal from 'sweetalert2';
import { EmpleadoService } from '../services/empleado.service';

@Component({
    selector: 'app-empnuevo',
    templateUrl: './empNuevo.component.html',
    styleUrls: ['./empNuevo.component.css'],
    providers: [EmpleadoService]
})
export class EmpNuevoComponent implements OnInit {
    //#region Variables
    public titulo: string;
    public empleado: Empleado;
    public toast = swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
    });
    //#endregion Variables

    //#region Constructores
    constructor(
        private _empleadoService: EmpleadoService
    ) {
        this.titulo = 'EmpNuevo';
        this.empleado = new Empleado('', '', '1', '1');
    }

    ngOnInit() { }
    //#endregion Constructores

    //#region Consultas
    crearEmpleado() {
        this._empleadoService.crearEmpleado(this.empleado).subscribe(
            response => {
                if (response.code === 200) {
                    swal({
                        type: 'success',
                        title: 'Número de empleado:',
                        text: response.data[0].clave
                    });
                } else {
                    this.toast({
                        type: 'error',
                        title: 'Error al guardar el nuevo empleado.'
                    });
                }
            },
            error => {
                this.toast({
                    type: 'error',
                    title: 'Error al guardar el nuevo empleado.'
                });
            }
        );
    }
    //#endregion Consultas

    //#region Metodos Form
    onSubmitNuevo(form: NgForm) {
        this.empleado.nombre = this.empleado.nombre.toUpperCase();
        this.crearEmpleado();
        form.resetForm();
        this.reiniciarForm();
    }
    //#endregion Metodos Forms

    //#region Metodos
    reiniciarForm() {
        setTimeout(() => {
            this.empleado = new Empleado('', '', '1', '1');
        }, 10);
    }
    //#endregion Metodos

    //#region Metodos validadores de caracteres
    soloLetras(e: any) {
        const key = e.keyCode || e.which;
        const tecla = String.fromCharCode(key).toLowerCase();
        const letras = ' áéíóúabcdefghijklmnñopqrstuvwxyz';
        const especiales = ['8', '37', '39', '46'];

        let tecla_especial = false;
        especiales.forEach(x => {
            if (key === x) {
                tecla_especial = true;
            }
        });

        if (letras.indexOf(tecla) === -1 && !tecla_especial) {
            return false;
        }
    }
    //#endregion Metodos validadores de caracteres
}
