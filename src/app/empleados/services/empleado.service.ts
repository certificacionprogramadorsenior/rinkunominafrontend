import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable, Subject, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { Empleado } from '../models/empleado';
import { GLOBAL } from '../../services/global';

@Injectable()
export class EmpleadoService {
    public url: string;

    constructor(
        public _http: Http
    ) {
        this.url = GLOBAL.url;
    }

    crearEmpleado(empleado: Empleado) {
        const json = JSON.stringify(empleado);
        const params = 'json=' + json;
        const headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});

        return this._http.post(this.url + 'crear/empleado', params, {headers: headers}).pipe(map(res => res.json()));
    }

    consultarEmpleados() {
        return this._http.get(this.url + 'consultar/empleados').pipe(map(res => res.json()));
    }

    consultarEmpleado(numero: string) {
        return this._http.get(this.url + 'consultar/empleado/' + numero).pipe(map(res => res.json()));
    }

    actializarEmpleado(empleado: Empleado) {
        const json = JSON.stringify(empleado);
        const params = 'json=' + json;
        const headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});

        return this._http.post(this.url + 'actualiza/empleado', params, {headers: headers}).pipe(map(res => res.json()));
    }

    eliminarEmpleado(numero: string) {
        return this._http.get(this.url + 'elimina/empleado/' + numero).pipe(map(res => res.json()));
    }
}
