import { Component, OnInit } from '@angular/core';
import { Empleado } from '../models/empleado';
import swal from 'sweetalert2';
import { EmpleadoService } from '../services/empleado.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
    selector: 'app-empconsulta',
    templateUrl: './empConsulta.component.html',
    styleUrls: ['./empConsulta.component.css'],
    providers: [EmpleadoService]
})
export class EmpConsultaComponent implements OnInit {
    //#region Variables
    public titulo: string;
    public empleado;
    public datosCompletos: Array<Empleado>;
    public empleados: Array<Empleado>;
    public toast = swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
    });
    public swalWithBootstrapButtons = swal.mixin({
        confirmButtonClass: 'btn btn-success separa_boton',
        cancelButtonClass: 'btn btn-danger separa_boton',
        buttonsStyling: false,
    });
    public claveFiltro: string;
    public nombreFiltro: string;
    public rolFiltro: string;
    public tipoFiltro: string;
    //#endregion Variables

    //#region Constructores
    constructor(
        private _empleadoService: EmpleadoService,
        private _route: ActivatedRoute,
        private _router: Router
    ) {
        this.titulo = 'EmpConsulta';
        this.claveFiltro = '';
        this.nombreFiltro = '';
        this.rolFiltro = '';
        this.tipoFiltro = '';
    }

    ngOnInit() {
        this.consultarEmpleados();
    }
    //#endregion Contructores

    //#region Consultas
    consultarEmpleados() {
        this._empleadoService.consultarEmpleados().subscribe(
            response => {
                if (response.code === 200) {
                    this.datosCompletos = response.data;
                    this.empleados = this.datosCompletos;
                } else {
                    this.toast({
                        type: 'error',
                        title: 'Error al consultar los empleados.'
                    });
                }
            },
            error => {
                this.toast({
                    type: 'error',
                    title: <any>error
                });
            }
        );
    }

    eliminarEmpleado() {
        this._empleadoService.eliminarEmpleado(this.empleado.clave).subscribe(
            response => {
                if (response.code === 200 && response.data) {
                    this.toast({
                        type: 'info',
                        title: 'Empleado eliminado correctamente.'
                    });
                    this.borrarDeDatosCompletos();
                    this.empleados.splice(this.empleado.lugar, 1);
                } else {
                    this.toast({
                        type: 'error',
                        title: 'Error al eliminar el empleado.'
                    });
                }
            },
            error => {
                this.toast({
                    type: 'error',
                    title: <any>error
                });
            }
        );
    }
    //#endregion Consultas

    //#region Metodos Front
    preguntarBorrar(index) {
        this.empleado = this.empleados[index];
        this.empleado.lugar = index;

        this.swalWithBootstrapButtons({
            title: 'Está seguro?',
            html:
                '<div style="font-size:1.5rem;">' +
                'Clave de empleado: <strong>' + this.empleado.clave + '</strong><br>' +
                'Nombre: <strong>' + this.empleado.nombre + '</strong><br>' +
                'Rol: <strong>' + this.empleado.rol + '</strong><br>' +
                'Tipo: <strong>' + this.empleado.tipo + '</strong><br>' +
                '</div>',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Si, borralo!',
            cancelButtonText: 'No, cancela!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                this.eliminarEmpleado();
            }
        });
    }

    recontruir(cadena: string, cual: number) {
        this.empleados = [];

        switch (cual) {
            case 1:
                this.claveFiltro = cadena;
                break;
            case 2:
                cadena = cadena.toUpperCase();
                this.nombreFiltro = cadena;
                break;
            case 3:
                this.rolFiltro = cadena;
                break;
            case 4:
                this.tipoFiltro = cadena;
                break;
        }
        this.datosCompletos.forEach(emp => {
            if (emp.clave.includes(this.claveFiltro) &&
                emp.nombre.includes(this.nombreFiltro) &&
                emp.rol.includes(this.rolFiltro) &&
                emp.tipo.includes(this.tipoFiltro)
            ) {
                this.empleados.push(emp);
            }
        });
    }
    //#endregion Metodos Front

    //#region Metodos
    borrarDeDatosCompletos() {
        for (let i = 0; i < this.datosCompletos.length; i++) {
            if (this.datosCompletos[i].clave === this.empleado.clave) {
                this.datosCompletos.splice(i, 1);
                break;
            }
        }
    }
    //#endregion Metodos

    //#region Metodos validadores de caracteres
    soloNumeros(e: any) {
        const key = window.event ? e.which : e.keyCode;
        if (key < 48 || key > 57) {
            e.preventDefault();
        }
    }

    soloLetras(e: any) {
        const key = e.keyCode || e.which;
        const tecla = String.fromCharCode(key).toLowerCase();
        const letras = ' áéíóúabcdefghijklmnñopqrstuvwxyz';
        const especiales = ['8', '37', '39', '46'];

        let tecla_especial = false;
        especiales.forEach(x => {
            if (key === x) {
                tecla_especial = true;
            }
        });

        if (letras.indexOf(tecla) === -1 && !tecla_especial) {
            return false;
        }
    }
    //#endregion Metodos validadores de caracteres
}
