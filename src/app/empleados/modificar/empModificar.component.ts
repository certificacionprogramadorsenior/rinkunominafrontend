import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Empleado } from '../models/empleado';
import swal from 'sweetalert2';
import { EmpleadoService } from '../services/empleado.service';

@Component({
    selector: 'app-empmodificar',
    templateUrl: './empModificar.component.html',
    styleUrls: ['./empModificar.component.css'],
    providers: [EmpleadoService]
})
export class EmpModificarComponent implements OnInit {
    //#region Variables
    public id: string;
    public titulo: string;
    public empleado: Empleado;
    public toast = swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
    });
    //#endregion Variables

    //#region Constructores
    constructor(
        private _empleadoService: EmpleadoService,
        private _route: ActivatedRoute,
        private _router: Router
    ) {
        this.empleado = new Empleado('', '', '', '');
    }

    ngOnInit() {
        this.consultarEmpleado();
    }
    //#endregion Constructores

    //#region Consultas
    consultarEmpleado() {
        this._route.params.forEach((params: Params) => {
            this.id = params['id'];
        });
        this._empleadoService.consultarEmpleado(this.id).subscribe(
            response => {
                if (response.code === 200) {
                    this.empleado = response.data[0];
                } else {
                    this.toast({
                        type: 'error',
                        title: 'Error al consultar el empleados.'
                    });
                }
            },
            error => {
                this.toast({
                    type: 'error',
                    title: <any>error
                });
            }
        );
    }

    actializarEmpleado() {
        this._empleadoService.actializarEmpleado(this.empleado).subscribe(
            response => {
                if (response.code === 200 && response.data) {
                    swal({
                        type: 'success',
                        title: 'Empleado actualizado correctamente',
                        timer: 2500
                    });
                    setTimeout(() => {
                        this._router.navigate(['/empleado/buscar']);
                    }, 2700);
                } else {
                    this.toast({
                        type: 'error',
                        title: 'Error actualizando el Empleado.'
                    });
                }
            },
            error => {
                this.toast({
                    type: 'error',
                    title: <any>error
                });
            }
        );
    }
    //#endregion Consultas

    //#region Metodos Front
    onSubmitActualizar() {
        this.empleado.nombre = this.empleado.nombre.toUpperCase();
        this.actializarEmpleado();
    }
    //#endregion Metodos Front

    //#region Metodos validadores de caracteres
    soloLetras(e: any) {
        const key = e.keyCode || e.which;
        const tecla = String.fromCharCode(key).toLowerCase();
        const letras = ' áéíóúabcdefghijklmnñopqrstuvwxyz';
        const especiales = ['8', '37', '39', '46'];

        let tecla_especial = false;
        especiales.forEach(x => {
            if (key === x) {
                tecla_especial = true;
            }
        });

        if (letras.indexOf(tecla) === -1 && !tecla_especial) {
            return false;
        }
    }
    //#endregion Metodos validadores de caracteres

}
